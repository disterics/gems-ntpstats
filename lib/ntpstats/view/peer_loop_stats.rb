require 'ntpstats/view/view'

module Ntpstats::View
  class PeerLoopStats < View

    def initialize(peer_stats, loop_stats, key)
      @data = nil
      @key = key
      @loop_stats = loop_stats
      @peer_stats = peer_stats
      combine(@peer_stats, @loop_stats, @key)
    end

    private

    def combine(peer_stats, loop_stats, key)
      peers = peers_map(peer_stats)
      num_cols = peers.size + 1
#      @titles = ['TimeStamp', 'Frequency'].concat(Array.new(peers_map.size, nil))
      ts_hash = Hash.new
      loop_stats.each do | stat |
        ts_hash[stat.ts] ||= Array.new(num_cols, nil)
        ts_hash[stat.ts][0] = stat.frequency
      end

      peer_stats.each do | stat |
        ts_hash[stat.ts] ||= Array.new(num_cols, nil)
        idx = peers[stat.address]
        ts_hash[stat.ts][idx] = stat.send(key)
      end
      @data = ts_hash_to_array(ts_hash)
      @titles = ['TimeStamp', 'Frequency'].concat(Array.new(peers_map.size, nil))
      peers_map.each  { |k, v| titles[v+1] = k }
#      @data = peer_stats.collect { |s| [s.ts, s.send(key)] }
#      @titles = ['TimeStamp', key]
    end

    def peers_map(peer_stats = nil)
      peer_stats ||= @peer_stats
      k = 0
      peer_stats.reduce(Hash.new) do | memo, stat|
        k += 1 unless memo.key?(stat.address)
        memo[stat.address] = k unless memo.key?(stat.address)
        memo
      end
    end
  end
end