module Ntpstats::View
  class View

    include Enumerable
    attr_reader :data, :titles

    def each(&block)
      @data.each do | datum |
        if block_given?
          block.call(datum)
        else
          yield datum
        end
      end
    end

    private
    def ts_hash_to_array(hash)
      arr = hash.collect do |k, a|
        [k].concat(a)
      end
      arr.sort { |a, b| a[0] <=> b[0]}
    end
  end
end