module Ntpstats::View
  class MultiLoopStats

    include Enumerable
    attr_reader :data, :titles

    def initialize(loop_stats, key)
      @data = nil
      @key = key
      @loop_stats = loop_stats
      interpolate
      gen_titles(loop_stats)
    end

    def each(&block)
      @data.each do | datum |
        if block_given?
          block.call(datum)
        else
          yield datum
        end
      end
    end

    private

    def interpolate
      @data = timeSeries(@key) if @data.nil?
      # @data.each { |d| puts "Entry #{d}" }
    end

    def gen_titles(loop_stats)
      @titles = ['TimeStamp']
      @titles.concat(loop_stats.collect { |l| l.name })
    end

    def timeSeries(key)
      num_sources = @loop_stats.count
      ts_hash = Hash.new
      i = 0
      @loop_stats.each do | ls |
        ts = ls.timeSeries(@key)
        ts.each do |k, v|
          ts_hash[k] = Array.new(num_sources, nil) if ts_hash[k].nil?
          a = ts_hash[k]
          a[i] = v
        end
        i += 1
      end
      ts = ts_hash.collect do |k, a|
        [k].concat(a)
      end
      ts.sort! { |a, b| a[0] <=> b[0]}

      cur_val = @loop_stats.collect { |loop_stat| loop_stat.first(@key) }
#      puts " Top values: #{cur_val}"
      ts.collect! do | d |
#        puts "Array is #{d}"
        entry = [d[0]]
        num_sources.times do |i|
          entry << (d[i+1] || cur_val[i])
#          d[i+i] = cur_val[i] if d[i+1].nil?
          cur_val[i] = d[i+1] unless d[i+1].nil?
        end
#        puts "Entry #{entry}"
        entry
      end
      ts
      # ts = ts_hash.collect do |k, a|
      #   entry = [k]
      #   puts "Array is #{a}"
      #   num_sources.times do |i|
      #     entry << (a[i] || cur_val[i])
      #     cur_val[i] = a[i] unless a[i].nil?
      #   end
      #   puts "Entry #{entry}"
      #   entry
      # end
      # ts.sort { |a, b| a[0] <=> b[0]}
    end

  end
end