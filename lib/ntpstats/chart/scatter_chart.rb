require 'erb'
require 'json'

require 'ntpstats/chart/chart'

module Ntpstats::Chart
  class ScatterChart < Chart
    class << self
      def draw(name, data)
        lc = ScatterChart.new(name, data)
        lc.save()
      end
    end

    TEMPLATE_FILE = 'scatter_chart.html.erb'

  end
end