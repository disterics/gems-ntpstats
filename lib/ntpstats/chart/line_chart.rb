require 'erb'
require 'json'

require 'ntpstats/chart/chart'

module Ntpstats::Chart
  class LineChart < Chart
    class << self
      def draw(name, data)
        lc = LineChart.new(name, data)
        lc.save()
      end
    end

    TEMPLATE_FILE = 'line_chart.html.erb'

    # attr_accessor :name, :json_data

    # def initialize(name, data)
    #   @name = name
    #   @data_source = data
    #   @json_data = ''
    # end

    # private
    # def to_json()
    #   data = Array.new
    #   data << @data_source.titles
    #   @data_source.each { |d| data << d }
    #   # data.concat(@data_source.data)
    #   @json_data = data.to_json
    #   # numEntries = @data_sources.max { |a, b| a.length <=> b.length }.length
    #   # numEntries.times do |i|
    #   #   data << [i].concat(@data_sources.collect { |d| d.at(i, @key)  })
    #   # end
    #   # ts_hash =  Hash.new
    #   # i = 0
    #   # @data_sources.each do | ds |
    #   #   ts = ds.timeSeries(@key)
    #   #   ts.each do |k, v|
    #   #     ts_hash[k] = Array.new(@data_sources.count, nil) if ts_hash[k].nil?
    #   #     a = ts_hash[k]
    #   #     a[i] = v
    #   #   end
    #   #   i += 1
    #   # end
    #   # ts_hash.each do |ts, values|
    #   #   data << [ts].concat(values)
    #   # end
    # end

    # def render
    #   to_json
    #   erb = ERB.new(File.read(template())).result(binding)
    #   erb
    # end

  end
end