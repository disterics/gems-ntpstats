module Ntpstats::Chart
  class Chart

    TEMPLATES_DIR = 'templates'

    attr_accessor :name, :json_data

    def initialize(name, data)
      @name = name
      @data_source = data
      @json_data = ''
    end

    def save
#      render
#      return
      t = Tempfile.new([name, '.html'])
      path = t.path
      t.close
      t.unlink
      File.open(path, 'w') do |file|
        file.write(render)
      end
      path
    end

    private
    def template
      File.join(File.dirname(__FILE__), TEMPLATES_DIR, template_file)
    end

    def template_file
      self.class::TEMPLATE_FILE
    end

    def to_json()
      data = Array.new
      data << @data_source.titles
      @data_source.each { |d| data << d }
      @json_data = data.to_json
    end

    def render
      to_json
      erb = ERB.new(File.read(template())).result(binding)
      erb
    end

  end
end
