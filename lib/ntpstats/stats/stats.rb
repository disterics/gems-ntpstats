module Ntpstats::Stats
  class Stats

    include Enumerable

    class << self
      def open(dir)
        new(dir)
      end
    end

    LINE_SPLIT_REGEX = /\s+/

    attr_reader :data, :name

    def initialize(dir)
      @data = Array.new
      @dir = dir
      @name = File.basename(dir)
      files = Dir.glob(File.join(dir, glob_pattern))
      read(files)
    end

    def each(&block)
      c = 0
      @data.each do | datum |
        if block_given?
          block.call(datum)
        else
          yield datum
        end
        c += 1
#        break if c > 9
      end
    end


    def first(key)
      @data.first.send(key)
    end

    def timeSeries(key)
      series = Hash.new
      @data.each do |d|
        series[d.ts] = d.send(key)
#       c += 1
#       break if c > 20
      end
      series
    end

    def length
      @data.count
    end

    private

    def read(files)
      files.each do |file|
        # puts "file:#{loop_file}"
        File.open(file) do |f|
          f.each_line do | line |
            statistic = process_line(line.split(LINE_SPLIT_REGEX))
            put "error: invalid line #{line}" if statistic.invalid?
            @data << statistic.data unless statistic.invalid?
          end
        end
      end
      @data.sort! { |a, b| a.ts <=> b.ts }
    end

    def create_timestamp(day, seconds)
      DateTime.jd(2400000 + day, (seconds / (60*60)))
    end

    def glob_pattern
      self.class::GLOB_PATTERN
    end

  end
end
