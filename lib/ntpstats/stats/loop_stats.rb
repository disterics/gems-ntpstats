require 'ostruct'

require 'ntpstats/stats/stats'

module Ntpstats::Stats
	class LoopStats < Stats

		NUM_WORDS = 7

		GLOB_PATTERN = 'loops.*'

		private

		def process_line(words)
			result = OpenStruct.new(:invalid? => (words.count != NUM_WORDS))
			result.data = build(words) unless result.invalid?
			result
		end

		def build(words)
			day = words[0].to_i
			seconds = words[1].to_f
			ts = create_timestamp(day, seconds)
			OpenStruct.new({:day => day, :seconds => seconds, :time => words[2].to_f,
				:frequency => words[3].to_f, :jitter => words[4].to_f, :deviation => words[5].to_f, :descipline => words[6].to_f, :ts => ts})
		end

# 		class << self
# 			def open(dir)
# 				new(dir)
# 			end
# 		end

# 		def initialize(dir)
# 			@data = Array.new
# 			@name = File.basename(dir)
# 			# puts "dir: #{dir}"
# 			glob_pattern = File.join(dir, 'loops.*')
# 			# puts "pat: #{glob_pattern}"
# 			loop_files = Dir.glob(glob_pattern)
# 			# puts "pat: #{loop_files}"
# 			read(loop_files)
# 		end

# 		def read(loop_files)
# 			loop_files.each do |loop_file|
# 				# puts "file:#{loop_file}"
# 				File.open(loop_file) do |f|
# 					f.each_line do | line |
# 						words = line.split(/\s+/)
# 						puts "error: incomplete line #{line}" if words.count != 7
# 						@data << build(words) unless words.count != 7
# 					end
# 				end
# 			end
# 			@data.sort! { |a, b| a.ts <=> b.ts }
# 		end

# 		def first(key)
# 			@data.first.send(key)
# 		end

# 		def at(i, key)
# 			data = @data.at(i)
# 			data.send(key) unless data.nil?
# 		end

# 		def timeSeries(key)
# 			series = Hash.new
# #			c = 0
# 			@data.each do |d|
# 				series[d.ts] = d.send(key)
# #				c += 1
# #				break if c > 20
# 			end
# 			series
# 		end

# 		def name
# 			@name
# 		end

# 		def length
# 			@data.count
# 		end

	end
end

