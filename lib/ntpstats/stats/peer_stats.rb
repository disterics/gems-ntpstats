require 'ostruct'

module Ntpstats::Stats
	class PeerStats < Stats

		NUM_WORDS = 8
		SELECT_ARR = ['<', 'x', '.', '-', '+', '#', '*', 'o']
		EVENT_ARR = %w(zero mobilize demobilize unreachable reachable restart no_reply rate_exceeded access_denied leap_armed sys_peer clock_event
			bad_auth popcorn interleave_mode interleave_error)

		GLOB_PATTERN = 'peers.*'

		def at(i, key)
			data = @data.at(i)
			data.send(key) unless data.nil?
		end

		private
		def build(words)
			day = words[0].to_i
			seconds = words[1].to_f
			status_word = words[3]
			status = decode_status(status_word)
			ts = create_timestamp(day, seconds)
			OpenStruct.new({:day => day, :seconds => seconds, :address => words[2],
							:status_hex => status_word, :offset => words[4].to_f, :delay => words[5].to_f,
							:dispersion => words[6].to_f, :jitter => words[7], :status => status, :ts => ts})
		end

		def process_line(words)
			result = OpenStruct.new(:invalid? => (words.count != NUM_WORDS))
			result.data = build(words) unless result.invalid?
			result
		end

		def decode_status(status_word)
			hex_value = status_word.to_i(16)
			status_bits =  ((hex_value & 0xF800) >> 11)
			select_word = ((hex_value & 0x0700) >> 8)
			count = ((hex_value & 0x00F0) >> 4)
			code_word = (hex_value & 0xF)
			status = decode_status_bits(status_bits)
			select = decode_select_word(select_word)
			code = decode_code_word(code_word)
			"#{select} #{status}, #{count} x #{code}"
		end

		def decode_status_bits(bits)
			bcst = 'bcst' if (bits & 0x01) != 0
			reach = 'reachable' if (bits & 0x02) != 0
			authenb = 'authenb' if (bits & 0x04) != 0
			auth = 'auth' if (bits & 0x08) != 0
			config = 'persistent' if (bits & 0x10) != 0
			[bcst, reach, authenb, auth, config].reduce('') do |memo, value|
				memo += ', ' unless memo.empty? || value.nil?
				memo += value unless value.nil?
				memo
			end
		end

		def decode_select_word(code)
			SELECT_ARR[code]
		end

		def decode_code_word(code)
			EVENT_ARR[code]
		end

	end
end

