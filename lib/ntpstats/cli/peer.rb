require 'ntpstats/cli/command'
require 'ntpstats/view/peer_loop_stats'

module Ntpstats::Cli
  class Peer < Command
    desc "chart <dir>", "Charts the loopstats and peerstats data in the given directory"
    long_desc <<-LONGDESC
      Uses google charts API to chart the loopstats and peersstats data in the gievn directory.
      Defaults to charting the frequency offset over time.

      With -k <key> option, charts the given data points.
      Possible values of <key> are time, frequency, jitter, deviation, desciplime

    LONGDESC
    option :key, :desc => "One of offset, delay, dispersion, jitter", :default => "offset", :aliases => [:k]
    def chart(dir)
      loop_stats = get_loop_stats(dir)
      peer_stats = get_peer_stats(dir)
      data = create_combined_peer_view(peer_stats, loop_stats, options[:key])
      chart = create_scatter_chart(options[:key], data)
      show_chart(chart)
    end

    private

    def create_combined_peer_view(peer_stat, loop_stat, key)
      Ntpstats::View::PeerLoopStats.new(peer_stat, loop_stat, key)
    end
  end
end