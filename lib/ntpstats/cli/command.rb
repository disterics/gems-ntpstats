require 'launchy'

require 'ntpstats/chart/line_chart'
require 'ntpstats/chart/scatter_chart'
require 'ntpstats/stats/loop_stats'
require 'ntpstats/stats/peer_stats'

module Ntpstats::Cli
  class Command < Thor

    private
    def get_loop_stats(dir)
      Ntpstats::Stats::LoopStats.open(dir)
    end

    def get_peer_stats(dir)
      Ntpstats::Stats::PeerStats.open(dir)
    end

    def show_chart(chart)
      Launchy.open(URI.join('file:///', chart))
    end

    def create_line_chart(name, data)
      Ntpstats::Chart::LineChart.draw(name, data)
    end

    def create_scatter_chart(name, data)
      Ntpstats::Chart::ScatterChart.draw(name, data)
    end

  end
end
