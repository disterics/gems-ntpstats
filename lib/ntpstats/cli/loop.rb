require 'ntpstats/cli/command'
require 'ntpstats/view/multi_loop_stats'

module Ntpstats::Cli
  class Loop < Command
    desc "chart <dirs>", "Charts the loopstats data in the given directory"
    long_desc <<-LONGDESC
      Uses google charts API to chart the loopstats data in the gievn directory.
      Defaults to charting the frequency offset over time.

      With -k <key> option, charts the given data points.
      Possible values of <key> are time, frequency, jitter, deviation, desciplime

    LONGDESC
    option :key, :desc => "One of time, frequency, jitter, deviation, descipline", :default => "frequency", :aliases => [:k]
    def chart(*dirs)
      loops = dirs.collect { |dir| get_loop_stats(dir) }
      data = create_combined_loop_view(loops, options[:key])
      chart = create_line_chart(options[:key], data)
      show_chart(chart)
    end

    private

    def create_combined_loop_view(loops, key)
      Ntpstats::View::MultiLoopStats.new(loops, key)
    end

  end
end