require 'thor'

require 'ntpstats/cli/loop'
require 'ntpstats/cli/peer'

module Ntpstats
	class CLI < Thor
		desc "loop SUBCOMMAND ...DIRS", "process loop statistics"
		subcommand "loop", Cli::Loop

    desc "peer SUBCOMMAND DIR", "process peer statistics"
    subcommand "peer", Cli::Peer
	end
end
