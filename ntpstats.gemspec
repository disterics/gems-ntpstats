# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'ntpstats/version'

Gem::Specification.new do |spec|
  spec.name          = "ntpstats"
  spec.version       = Ntpstats::VERSION
  spec.authors       = ["Henry Barnor"]
  spec.email         = ["henry.barnor@dolby.com"]
  spec.summary       = %q{Parse and manipulate ntp statistics files.}
  spec.description   = %q{Parse and manipulate ntp statistics files.}
  spec.homepage      = "http://confluence.dolby.net/kb/display/~hbarn"
  spec.license       = "Private Unlicensed"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_dependency "thor"
  spec.add_dependency "launchy"

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rspec"
  spec.add_development_dependency "guard-rspec"
  spec.add_development_dependency "ruby_gntp"

end
